﻿#include <iostream>

int SumOfNumbers(int n)
{
	if (n < 10)
		return n;
	return (n % 10) + SumOfNumbers(n / 10);
}

int main()
{
	int n;
	std::cin >> n;
	std::cout << SumOfNumbers(n);
}


