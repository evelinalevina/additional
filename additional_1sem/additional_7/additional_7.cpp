﻿
#include <vector>
#include <algorithm>
#include <iostream>


int main()
{
    int s, k, n, a;
    std::vector <int> vec;
    std::cin >> n;
    for (int i = 0; i < n; i++)
    {
        std::cin >> a;
        vec.push_back(a);
    }

    for (int i = 0; i < vec.size(); i++)
    {
        s = vec[i];
        int sum = 0;

        while (s > 0)
        {
            k = s % 10;
            s = s / 10;
            sum += k;
        }

        if (sum == 18) 
        {
            vec.erase(vec.begin() + i);
        }
    }
    int size = vec.size();
    for (int i = 0; i < size; i++) 
    {
        s = vec[i];
        int mult = 1;

        while (s > 0)
        {
            k = s % 10;
            s = s / 10;
            mult *= k;
        }

        if (mult == 35)
        {
            vec.insert(vec.begin() + i,  vec[i]);
            i++;
        }
    }
    for (int i = 0; i < vec.size(); i++)
        std::cout << vec[i] << " ";
    return 0;
}


