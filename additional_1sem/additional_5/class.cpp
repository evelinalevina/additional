#pragma once
#include <iostream>
#include "class.hpp"

namespace el {
    void Read(int& n, int arr[10000]) {
        for (int i = 0; i < n; i++)
            std::cin >> arr[i];
    }

    void Main(int& n, int arr[10000], int& s, int& sum, int& mult, int& k) {
        for (int i = 0; i < n; i++)
        {
            s = arr[i];
            mult = 1;
            sum = 0;

            while (s > 0)
            {
                k = s % 10;
                s = s / 10;
                sum += k;
                mult *= k;
            }
            if (sum == 18)
            {
                for (int j = i; j <= n - 1; j++)
                    arr[j] = arr[j + 1];
                n--;
                i--;
            }
            if (mult == 35)
            {
                n++;
                for (int j = n - 1; j >= i + 1; j--)
                    arr[j] = arr[j - 1];
                i++;
            }
        }
    }

    void Write(int& n, int arr[10000]) {
        for (int i = 0; i < n; i++)
            std::cout << arr[i] << " ";
    }
}
