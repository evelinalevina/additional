﻿#include <iostream>

int main()
{
	setlocale(LC_ALL, "Rus");
	int Q1, P1, Q2, P2, A;
	int min = 100000;
	std::cin >> Q1 >> P1 >> Q2 >> P2 >> A;
	if ((Q1 >= 1000) || (P1 >= 1000) || (Q2 >= 1000) || (P2 >= 1000) || (A >= 1000))
	{
		std::cout << "Ошибка! Введены слишком большие значения!";
		return 0;
	}
	for (int i = 0; i <= 1000; i++) {
		for (int j = 0; j <= 1000; j++) {
			if ((i * Q1 + j * Q2 >= A) && (i * P1 + j * P2 < min) && (i * P1 + j * P2 > 0))
				min = i * P1 + j * P2;
		}
	}
	std::cout << min;
	return 0;


}


