﻿#include <iostream>
#include "class.hpp"

int main()
{
	el::Circle circle;
	int R;
	circle.Read();
	circle.CheckR();
	std::cout << "Square = " << circle.Square() << std::endl;
	std::cout << "Perimeter = " << circle.Perimeter() << std::endl;
	return 0;
}



