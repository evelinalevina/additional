#include "class.hpp"
#include <iostream>
#define PI acos(-1)

namespace el
{
	Circle::Circle() {};

	Circle::~Circle() { }

	double Circle::Square()
	{
		return m_R * m_R * PI; 
	}

	double Circle::Perimeter()
	{ 
		return 2 * m_R * PI; 
	}

	void Circle::CheckR() 
	{
		if (m_R <= 0)
		{
			std::cout << "R<=0" << std::endl;
			m_R = 1;
		}
	} 

	void Circle::Read()
	{
		int R;
		std::cout << "R = ";
		std::cin >> R;
		m_R = R;	
	}
}
