#pragma once

namespace el
{
    
	class Circle 
	{
	public:
		Circle();
		~Circle();
		void Read();
		double Square();
		double Perimeter();
		void CheckR();
	private:
		int m_R;
	};

   
    
}
