﻿#include <iostream>

int main()
{
    int count = 0;
    for (int i = 1; i <= 999999; i++)
    {
        int sum123 = 0;
        int sum456 = 0;
        for (int j = 0; j < 3; ++j)
            sum123 = i / 1 % 10 + i / 10 % 10 + i / 100 % 10;
        for (int k = 3; k < 6; ++k)
            sum456 = i / 1000 % 10 + i / 10000 % 10 + i / 100000 % 10;
        if (sum123 == sum456)
            count++;
    }
    std::cout << count << std::endl;
    return 0;


}


