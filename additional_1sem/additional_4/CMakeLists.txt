cmake_minimum_required(VERSION 3.20.2)
project(additional_4)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_EXTENSIONS OFF)
set(CMAKE_CXX_STANDARD_REQUIRED TRUE)


add_executable(additional_4 additional_4.cpp)